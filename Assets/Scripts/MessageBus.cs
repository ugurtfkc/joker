﻿
public abstract class BotEvent { }

public static class MessageBus
{
    public static void Publish<T>(T evnt) where T : BotEvent
    {
        UniRx.MessageBroker.Default.Publish(evnt);
    }

    public static UniRx.IObservable<T> OnEvent<T>() where T : BotEvent
    {
        return UniRx.MessageBroker.Default.Receive<T>();
    }
}
