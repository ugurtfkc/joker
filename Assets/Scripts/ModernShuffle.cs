﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShuffleAlg
{
    private static System.Random rng = new System.Random();
    
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int m = rng.Next(n + 1);
            T value = list[m];
            list[m] = list[n];
            list[n] = value;
        }
    }

}
