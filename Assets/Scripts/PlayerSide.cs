﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class PlayCardEvent : BotEvent
{
    public PlayerSide Player { get; set; }
    public Card Card { get; set; }
}

public abstract class PlayerSide
{
    public List<Card> Hand;
    protected int score;
    public int PlayerID;
    public Transform HandSpawn;
    public bool isMyTurn;
    protected bool isBot;

    public virtual void RoundStart() { }

    public virtual void TurnStart() { }

    public virtual void PlayCard(Card playedCard) { }

    public virtual void DrawCard(Card newCard) { }

    public virtual void Update() { }
    
    public PlayerSide()
    {
        isMyTurn = false;

        MessageBus.OnEvent<RoundStartEvent>().Subscribe(evnt => RoundStart());
        MessageBus.OnEvent<TurnStartEvent>().Where
        (evnt => evnt.Player.PlayerID == PlayerID).Subscribe(evnt =>
        {
            isMyTurn = true;
            TurnStart();
        });

        MessageBus.OnEvent<PlayCardCommand>().Where
        (evnt => evnt.Player.PlayerID == PlayerID).Subscribe(evnt => 
        {
            PlayCard(evnt.Card);
        });
        MessageBus.OnEvent<PlayerGotScoreEvent>().Where
        (evnt => evnt.Player.PlayerID == PlayerID).Subscribe(evnt =>
        {
            score = evnt.Score;
        });
    }
    
}

