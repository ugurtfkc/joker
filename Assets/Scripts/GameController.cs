﻿using UnityEngine;
using UniRx;
using UnityEngine.UI;


public class GameController : MonoBehaviour
{

    public Text PlayerScoreText;
    public Text BotScoreText;
    public Text Remaning;
    private int playerScore;
    private int botScore;
    private int remainingCards;
    private bool isGameRunning;
  
    void Start()
    {
        remainingCards = 52;
        playerScore = 0;
        botScore = 0;
        isGameRunning = true;

        MessageBus.OnEvent<PlayerGotScoreEvent>().Subscribe(evnt =>
        {

            if (evnt.Player.PlayerID == 0)
            {
                playerScore += evnt.Score;
            }
            else
            {
                botScore += evnt.Score;
            }
        });

        MessageBus.OnEvent<DrawCardEvent>().Subscribe(evnt =>
        {
            remainingCards = evnt.Remaining;
        });

        MessageBus.OnEvent<GameEndEvent>().Subscribe(evnt =>
        {
            isGameRunning = false;
        });

    }

    void Update()  //displaying score for everyframe
    {
        PlayerScoreText.text = "Your Score: " + playerScore;
        BotScoreText.text = "Bot Score: " + botScore;

        if (isGameRunning)
            Remaning.text = "Remaining cards:  " + remainingCards;
        else
        {

            string winner = "";
            if (playerScore > botScore)
            {
                winner = "Player";
            }
            else
            {
                winner = "Bot";
            }

            Remaning.text = "Game End\n" + winner + " Wins!";
            
              
            
        }
    }
}

