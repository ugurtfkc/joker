﻿using System.Collections.Generic;
using UnityEngine;

public class CardRenderer : MonoBehaviour, IPoolObject
{
    public Card Data;
    private SpriteRenderer renderer;

    public GameObject Prefab
    {
        get
        {
            return Source.Load<GameObject>(SourceType.Card);
        }
        set
        {

        }
    }
    
    public void Init()
    {

    }

    void Start()
    {
        Init();
    }

    public void LoadData(Card data, bool isSecret = false)
    {
        Data = data;
        renderer = GetComponent<SpriteRenderer>();
        if (!isSecret)
        {
            renderer.sprite = Source.GetCardSprite(Data.Type, Data.Value);
        }
        else
        {
            renderer.sprite = Source.GetCardBackSprite();
        }
    }

    public void LoadData(CardType type, CardValue value, bool isSecret = false)
    {
        var data = new Card(type, value);
        LoadData(data, isSecret);
    }

    public void LoadCardBack()
    {
        Data = null;
        renderer = GetComponent<SpriteRenderer>();
        renderer.sprite = Source.GetCardBackSprite();
    }

}

