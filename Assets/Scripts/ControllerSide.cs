﻿
public class PlayCardCommand : BotEvent
{
    public Card Card { get; set; }
    public PlayerSide Player { get; set; }
}

public abstract class ControllerSide
{
    protected PlayerSide myPlayer;
    protected bool isPlayable;

    protected ControllerSide()
    {

    }

    public virtual void Update() { }
}

