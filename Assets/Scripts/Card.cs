﻿using System.Collections.Generic;
using UnityEngine;

public class Card
{
 public CardType Type { get; private set; }
    public CardValue Value { get; private set; }
    private string id;
    public string ID
    {
        get
        {
            return id;
        }
        private set
        {
            id = value;
        }
    }
    public Card()
    {

    }
    public Card(CardType type, CardValue value)
    {
        Type = type;
        Value = value;
        ID = CreateId(type, value);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
            return false;

        Card crd = (Card)obj;
        return (Type == crd.Type) && (Value == crd.Value);
    }

    public bool Equals(CardType type, CardValue value)
    {
        if (Type == type && Value == value)
            return true;
        else
            return false;
    }

    public static string CreateId(CardType type, CardValue value)
    {
        return value.ToString() + " of " + type.ToString();
    }

    public override int GetHashCode()
    {
        var hashCode = 341992079;
        hashCode = hashCode * -1521134295 + Type.GetHashCode();
        hashCode = hashCode * -1521134295 + Value.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(id);
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ID);
        return hashCode;
    }
}


public enum CardType
{
    Clubs = 0,
    Diamonds = 1,
    Hearts = 2,
    Spades = 3
}

public enum CardValue
{
    Ace = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Jack = 11,
    Queen = 12,
    King = 13
}




