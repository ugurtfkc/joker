﻿using UnityEngine;
using UniRx;

public class PlayerController : ControllerSide
{
    public PlayerController(PlayerSide player)
    {
        isPlayable = false;
        myPlayer = player;

        MessageBus.OnEvent<TurnStartEvent>().Where
        (evnt => evnt.Player.PlayerID == myPlayer.PlayerID).Subscribe(evnt =>
        {
            myPlayer = evnt.Player;
            isPlayable = true;
        });
    }

    public override void Update()
    {
        if (!isPlayable)
        {
            return;
        }
        base.Update();
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1 << LayerMask.NameToLayer("Cards")))
            {
                Debug.Log("Card clicked by " + myPlayer.PlayerID);
                var hitCard = hit.transform.GetComponent<CardRenderer>().Data;
                MessageBus.Publish(new PlayCardCommand()
                {
                    Player = myPlayer,
                    Card = hitCard,
                });
                isPlayable = false;
            }
        }
    }
}


