﻿using System.Collections.Generic;
using UnityEngine;

public static class Source
{

    public static GameObject CardPrefab;

    private static Dictionary<SourceType, string> ResourcePaths = new Dictionary<SourceType, string>();

    private static Sprite[] CardSprites;
    static Source()
    {
        ResourcePaths.Add(SourceType.Card, "Prefabs/Card");
        
        //using object pooling for one prefabs, the motivation is speed.
        CardPrefab = Load<GameObject>(SourceType.Card);

        CardSprites = Resources.LoadAll<Sprite>("Sprites/Cards");
        GameObject go = Resources.Load<GameObject>("Prefabs/Card");
        ObjectPool.Instance.AddToPool(go, 100);
    }

    public static Sprite GetCardSprite(CardType type, CardValue value)
    {
        return CardSprites[(int)type + (((int)value - 1) * 4)];
    }

    public static Sprite GetCardBackSprite()
    {
        return CardSprites[52];
    }

    public static T Load<T>(SourceType srcType) where T : Object
    {
        if (!ResourcePaths.ContainsKey(srcType))
        {
            Debug.LogError("Resource does not exist!! " + srcType);
            return null;
        }

        return Resources.Load<T>(ResourcePaths[srcType]);
    }
}

public enum SourceType
{
    None,
    Card
}
