﻿using UniRx;
using System;

public class BotController : ControllerSide
{
    private Card lastPlayedCard;
    public BotController(PlayerSide player)
    {
        isPlayable = false;
        myPlayer = player;

        MessageBus.OnEvent<TurnStartEvent>().Where
            (evnt => evnt.Player.PlayerID == myPlayer.PlayerID).Subscribe(evnt =>
            {
                myPlayer = evnt.Player;
                lastPlayedCard = evnt.TopCard;
                isPlayable = true;
            });
    }

    public override void Update()
    {
        IDisposable d = null;
        if (!isPlayable)
        {
            return;
        }
        base.Update();
        if (lastPlayedCard == null)
        {
            isPlayable = false;
           //bot random plays
            d = Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe(x =>
            {
                MessageBus.Publish(new PlayCardCommand()
                {
                    Player = myPlayer,
                    Card = myPlayer.Hand[UnityEngine.Random.Range(0, myPlayer.Hand.Count)],
                });
                d.Dispose();
            });
        }
        else
        {
            for (int i = 0; i < myPlayer.Hand.Count && isPlayable; i++)
            {
                if (myPlayer.Hand[i].Value == lastPlayedCard.Value)
                {
                    isPlayable = false;
                   
                    d = Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe(x =>
                    {
                        MessageBus.Publish(new PlayCardCommand()
                        {
                            Player = myPlayer,
                            Card = myPlayer.Hand[i],
                        });
                        d.Dispose();
                    });
                    break;
                }
            }
            if (isPlayable && myPlayer.Hand.Count > 0)
            {
                isPlayable = false;
                
                d = Observable.Timer(TimeSpan.FromSeconds(1f)).Subscribe(x =>
                {
                    MessageBus.Publish(new PlayCardCommand()
                    {
                        //Bot can play random card
                        
                    Player = myPlayer,
                        Card = myPlayer.Hand[UnityEngine.Random.Range(0, myPlayer.Hand.Count)],
                    });
                
                    d.Dispose();
                });
            }
        }
    }
}

