﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : PlayerSide
{
    public Player(int playerID, Transform spawn, bool isBot = false)
    {
        Hand = new List<Card>();
        PlayerID = playerID;
        HandSpawn = spawn;
        this.isBot = isBot;
        score = 0;
    }

    public override void RoundStart()
    {
        base.RoundStart();
        HandOrderer();
    }

    public override void PlayCard(Card playedCard)
    {
        Debug.Log(PlayerID + " played " + playedCard.ID);
        base.PlayCard(playedCard);
        Hand.RemoveAll(c => playedCard.ID == c.ID);
        HandOrderer();
        MessageBus.Publish(new PlayCardEvent() { Player = this, Card = playedCard });
        isMyTurn = false;
    }

    public void HandOrderer()
    {
        for (int i = 0; i < HandSpawn.childCount; i++)
        {
            GameObject.Destroy(HandSpawn.GetChild(i).gameObject);
        }

        for (int i = 0; i < Hand.Count; i++)
        {
            GameObject newCard = ObjectPool.Instance.PopFromPool(Source.CardPrefab, false, true);
            newCard.GetComponent<CardRenderer>().LoadData(Hand[i], isBot);
            newCard.transform.position = HandSpawn.transform.position + new Vector3(i * 2f, 0f, 0f);
            newCard.transform.SetParent(HandSpawn);
        }
    }

    public override void TurnStart()
    {
        base.TurnStart();
        Debug.Log("My Turn " + PlayerID);
    }
    
    public override void DrawCard(Card newCard) //draw a card then add to handlist
    {
        base.DrawCard(newCard);
        Hand.Add(newCard);
    }
    
    public override void Update()
    {
        base.Update();
    }
}