﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {


    public static AudioClip gamePistiSound;
    public static AudioClip gameHitSound;
    static AudioSource audioSrc;
	// Use this for initialization
	void Start () {
        gamePistiSound = Resources.Load<AudioClip>("pistiSound");
        gameHitSound = Resources.Load<AudioClip>("hitSound");

        audioSrc = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void PlaySound(string s)
    {
        switch (s)
        {
            case "pistiSound":
                audioSrc.PlayOneShot(gamePistiSound);
                break;
            case "hitSound":
                audioSrc.PlayOneShot(gameHitSound);
                break;
        }
       
    }
}
