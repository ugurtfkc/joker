﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;


public class RoundStartEvent : BotEvent { }

public class GameEndEvent : BotEvent { }

public class TurnStartEvent : BotEvent
{
    public PlayerSide Player { get; set; }
    public Card TopCard { get; set; }
}

public class PlayerGotScoreEvent : BotEvent
{
    public PlayerSide Player { get; set; }
    public int Score { get; set; }
}

public class GameManager : MonoBehaviour
{
    public GameObject CardPrefab;

    [SerializeField]
    private Transform playerSpawn;
    [SerializeField]
    private Transform botSpawn;
    [SerializeField]
    private Transform midSpawn;
    [SerializeField]
    private Transform deckSpawn;

    private Deck deck;
    private List<ControllerSide> allControllers;
    private List<PlayerSide> players;
    private List<Card> midCards;
    
    private int turnCounter;
    private int maxTurn;
    private PlayerSide yourTurn;
    private bool isGameRunning = true;
    private int roundScore;

    void Start()
    {
        deck = new Deck();
        players = new List<PlayerSide>();
        allControllers = new List<ControllerSide>();
        midCards = new List<Card>();

        
        turnCounter = 0;
        roundScore = 0;

        players.Add(new Player(0, playerSpawn));
        allControllers.Add(new PlayerController(players[0])); //represents player

        players.Add(new Player(1, botSpawn, true));
        allControllers.Add(new BotController(players[1])); //represents bot

        MessageBus.OnEvent<PlayCardEvent>().Where(evnt => evnt.Player.PlayerID == yourTurn.PlayerID).Subscribe(evnt =>
        {
            players[turnCounter % 2] = evnt.Player;
            CardPlayed(evnt.Card);
        });

        MessageBus.OnEvent<GameEndEvent>().Subscribe(evnt =>
        {
            isGameRunning = false;
        });

        GameStart();
    }

    void Update()
    {
        if (!isGameRunning)
        {
            return;
        }
        for (int i = 0; i < allControllers.Count; i++)
        {
            allControllers[i].Update();
        }

        for (int i = 0; i < players.Count; i++)
        {
            players[i].Update();
        }
       
    }

    private void GameStart()
    {
        for (int i = 0; i < 4; i++)
        {
            PutCardOnMid(deck.Draw());
        }
        if (deck.RemainingCount() > 0 && deckSpawn.childCount < 1)
        {
            GameObject newCard = ObjectPool.Instance.PopFromPool(CardPrefab, false, true);
            newCard.GetComponent<CardRenderer>().LoadCardBack();
            newCard.transform.SetParent(deckSpawn);
            newCard.transform.localPosition = Vector3.zero;
        }
        else
        {
            GameObject lastCard = deckSpawn.GetChild(0).gameObject;
            ObjectPool.Instance.PushToPool(ref lastCard);
        }

        yourTurn = players[0];

        HandsAreEmpty();
    }

    private void HandsAreEmpty()
    {
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                Card newCard = deck.Draw();
                if (newCard == null)
                {
                    MessageBus.Publish(new GameEndEvent());
                    return;
                }
                players[j].DrawCard(newCard);
            }
        }

        MessageBus.Publish(new RoundStartEvent());
        MessageBus.Publish(new TurnStartEvent() { Player = yourTurn, TopCard = midCards.Count > 0 ? midCards[midCards.Count - 1] : null });
    }

    private void CardPlayed(Card newCard)
    {
        Debug.Log("GameManager: " + yourTurn.PlayerID + " played " + newCard.ID);
        PutCardOnMid(newCard);
        CheckForScore();
        turnCounter++;
        yourTurn = players[turnCounter % 2];
        if (turnCounter % (2 * 4) == 0) // if 8 card played turn is over.
        {
            HandsAreEmpty();
        }
        else
        {
            MessageBus.Publish(new TurnStartEvent() { Player = yourTurn, TopCard = midCards.Count > 0 ? midCards[midCards.Count - 1] : null });
        }
    }

    private void PutCardOnMid(Card card)
    {
        if (card == null)
        {
            MessageBus.Publish(new GameEndEvent());
            return;
        }
        midCards.Add(card);
        if (midSpawn.childCount > 0)
        {
            midSpawn.GetChild(0).GetComponent<CardRenderer>().LoadData(card);
        }
        else
        {
            GameObject newCard = ObjectPool.Instance.PopFromPool(Source.CardPrefab, false, true);
            newCard.GetComponent<CardRenderer>().LoadData(card);
            newCard.transform.SetParent(midSpawn);
            newCard.transform.localPosition = Vector3.zero;
        }
    }


    private void CheckForScore()
    {
        var newCard = midCards[midCards.Count - 1];

        if (newCard.Value == CardValue.Two && newCard.Type == CardType.Clubs)
        {
            roundScore += 2;
        }
        else if (newCard.Value == CardValue.Ten && newCard.Type == CardType.Diamonds)
        {
            roundScore += 3;
        }
        else if (newCard.Value == CardValue.Ace || newCard.Value == CardValue.Jack)
        {
            roundScore += 1;
        }

        if (midCards.Count > 1)
        {
            var oldCard = midCards[midCards.Count - 2];

            if (newCard.Value == oldCard.Value || newCard.Value == CardValue.Jack)
            {
                Debug.Log("Hit");
                SoundManager.PlaySound("hitSound");

                if (midCards.Count == 2)
                {
                    Debug.Log("Pisti");
                    if (newCard.Value == CardValue.Jack)
                    {
                        if (oldCard.Value == CardValue.Jack)
                        {
                            Debug.Log("Pisti with Jack");
                            MessageBus.Publish(new PlayerGotScoreEvent() { Player = yourTurn, Score = 20 });
                            Debug.Log("Player " + yourTurn.PlayerID + " got 20");
                            SoundManager.PlaySound("pistiSound");
                        }
                        else
                        {
                            MessageBus.Publish(new PlayerGotScoreEvent() { Player = yourTurn, Score = roundScore });
                            Debug.Log("Player " + yourTurn.PlayerID + " got " + roundScore);
                        }
                    }
                    else
                    {
                        Debug.Log("Pisti");
                        MessageBus.Publish(new PlayerGotScoreEvent() { Player = yourTurn, Score = 10 });
                        Debug.Log("Player " + yourTurn.PlayerID + " got 10");
                        SoundManager.PlaySound("pistiSound");
                    }
                }
                else
                {
                    MessageBus.Publish(new PlayerGotScoreEvent() { Player = yourTurn, Score = roundScore });
                    Debug.Log("Player " + yourTurn.PlayerID + " got " + roundScore);
                }

                roundScore = 0;
                midCards.Clear();

                foreach (Transform child in midSpawn)
                {
                    GameObject.Destroy(child.gameObject);
                }
                
            }
        }
        

    }

}

