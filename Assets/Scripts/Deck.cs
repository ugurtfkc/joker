﻿using System.Collections.Generic;
using UnityEngine;

public class DrawCardEvent : BotEvent
{
    public int Remaining;
}

public class Deck
{
    
    private List<Card> cardPool;
    private List<Card> usedCards;

    public Deck(int numberOfDeck = 1)
    {
        cardPool = new List<Card>();
        usedCards = new List<Card>();

        for (int i = 0; i < numberOfDeck; i++)
        {
            foreach (CardType type in System.Enum.GetValues(typeof(CardType)))
            {
                foreach (CardValue value in System.Enum.GetValues(typeof(CardValue)))
                {
                    cardPool.Add(new Card(type, value));
                }
            }
        }

        Shuffle();
    }

    public void Shuffle()
    {
        cardPool.Shuffle();
    }

    public Card Draw()
    {
        if (RemainingCount() == 0)
        {
            return null;
        }

        int rnd = Random.Range(0, RemainingCount());

        return Draw(cardPool[rnd].Type, cardPool[rnd].Value);
    }

    public Card Draw(CardType type, CardValue value)
    {
        Card crd = new Card(type, value);

        foreach (Card card in cardPool)
        {
            if (card.Equals(crd))
            {
                usedCards.Add(card);
                cardPool.Remove(card);
                MessageBus.Publish(new DrawCardEvent() { Remaining = cardPool.Count });
                return crd;
            }
        }

        return null;
    }

    public int RemainingCount()
    {
        return cardPool.Count;
    }

}
